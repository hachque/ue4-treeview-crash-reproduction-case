# Unreal Engine 4.25 TreeView Crash Reproduction

This project demonstrates a crash in UE4.25 when a TreeView receives temporary UObject*s in it's "Get Item Children" callback.

After loading the project in the editor, hit `~` and run `gc.CollectGarbageEveryFrame 1` to get a reliable reproduction. Then, click Play in the editor and scroll the tree view.

## Crash Sequence

Roughly, the sequence of events looks something like this:

- The TreeView is set up and configured in UMG.
- The developer implements "Get Item Children", which uses "Construct Object from Class". This constructs something that derives UObject (in this test case, I derived UObject and just added a string property).
- The results of "Get Item Children" go into UMG, which then go into Slate. **STreeView does not increment GC references when it's item type is based on UObject**.
- The tree view might initalize a few entries (those on screen), and passes them valid UObject pointers, because GC hasn't run yet. The full list of results are stored in `STreeView`.
- Garbage collection runs at the end of frame. For entries not on screen, there are no UMG items pointing to the underlying UObject. These UObjects are now only referenced by `STreeView`, but have no remaining GC references (because `STreeView` is Slate and does not declare the cached items as `UPROPERTY()`).
- The GC frees the UObjects for items that are off-screen. STreeView continues to keep the pointers though, because it's not using `UPROPERTY()`.
- When you scroll down in the tree view, the tree view assigns the UObject pointers it has cached to the item UMGs, but **these pointers are invalid** because the GC has already freed them. They're able to be set on the item UMG fine, as long as you do nothing but assign them to a property in the item UMG (if you try to actually cast or operate on them in the "On List Item Object Set" callback, then you'll get a crash here instead).
- The text widget has it's text bound to a callback in blueprint. Slate calls the callback in order for rendering; this callback then tries to use the Object pointer (by casting it, etc.).
- The game / engine crashes, because you're now trying to do stuff with a pointer that's no longer valid. Worse still, the stack trace has (almost) no user debuggable code, since the call hierarchy goes straight from the engine, to Slate, to blueprint execution, and it dies inside the `Cast to TestObject` node.

## Proposed Fix

When `STreeView` is operating on an item type based on `UObject*`, it needs to correctly add references in the GC for all the items it gets from the `OnGetChildren` callback. This will prevent crashes.